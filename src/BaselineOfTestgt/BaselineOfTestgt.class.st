Class {
	#name : #BaselineOfTestgt,
	#superclass : #BaselineOf,
	#category : #BaselineOfTestgt
}

{ #category : #loading }
BaselineOfTestgt class >> loadLepiter [
	IceRepository registry  
		detect: [ :aRepository |
			aRepository name = 'testgt' ]
		ifFound: [ :aRepository | 
			| defaultDatabase currentProperties |
			defaultDatabase := LeDatabasesRegistry defaultLogicalDatabase.
			currentProperties := defaultDatabase properties.
			currentProperties addRegisteredDirectory: aRepository repositoryDirectory / 'lepiter'.
			defaultDatabase reload ]
		ifNone: [
			self inform: 'Repository not found.' ]
]

{ #category : #baseline }
BaselineOfTestgt >> baseline: spec [
	<baseline>
		^ spec for: #common do: [
			spec package: 'Testgt'
		]
]
